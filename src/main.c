/**************************************************************************************************/
/*
 * File: main.c
 * Author: Kevin Klug
 * Date: 15 November 2011
 *
 * Description: source file for main() function implementation of ECE 275 
 *              Assignment 3 program dupes.
 *
 */
/**************************************************************************************************/
#include "bistree.h"
#include "bitree.h"
#include "global.h"
#include "thenecessary.h"
#include <stdio.h>


int main( int argc, char *argv[]){
    
    /* 
     * check for the correct number of commandline arguments. If incorrect
     * provide a simple usage message to the assit the user 
     */
	if( argc != 2 ) {
		printf("\nUsage: yeoldecalc inputfile \n\n");
        return -1;
	}
    
    //Create a binary tree
    BisTree DasTree;
    
    //Initialize Binary Tree
    bitree_init(&DasTree);
    
    if (yeoldecalc(argv[1], &DasTree) != TRUE) {
        printf("ERROR: Formatting issue\n");
    }
    
    //Destroy the Binary Tree
    bitree_destroy(&DasTree);
    
	return 0;
}

