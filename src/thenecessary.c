
/**************************************************************************************************/
/*
 * File: thenecessary.c
 * Author: Kevin Klug
 * Date: 10 November 2011
 *
 * Description: High level functions to implement the yeoldecalc program
 *
 */
/**************************************************************************************************/

#include "thenecessary.h"
#include "bistree.h"
#include <stdio.h>
#include <string.h>
#include <math.h>


bool yeoldecalc( char *inputFileName, BiTree *Tree ){
    
    FILE *inputFile;
    char line[10000];
    char firstWord[BIGGESTWORD];
    char secondWord[BIGGESTWORD];
    char thirdWord[BIGGESTWORD];
    char fourthWord[BIGGESTWORD];
    char fifthWord[BIGGESTWORD];
    char sixthWord[BIGGESTWORD];
    char seventhWord[BIGGESTWORD];
    char eighthWord[BIGGESTWORD];
    
    int i = 0;
    char operandName[BIGGESTWORD];
    char operatorOneName[BIGGESTWORD];
    char operatorTwoName[BIGGESTWORD];
    char operation[BIGGESTWORD];
    
    BiTreeNode *operandNode;
    BiTreeNode *operatorOneNode;
    BiTreeNode *operatorTwoNode;
    
    //int operandValue = 0;
    int operatorOneValue = 0;
    int operatorTwoValue = 0;
    
    //Open File
    inputFile = fopen(inputFileName, "r");
    
    //Check if file exists
    if(inputFile == NULL){
        printf("Error: Cannot open file.\n");
        return FALSE;
    }
    
    //Scan in a line from the input file
    while(fgets(line, sizeof(line), inputFile) != NULL){
        
        //San in the first word of the line
        if(sscanf(line, "%s %s %s %s %s %s %s %s", firstWord, secondWord, thirdWord, fourthWord, fifthWord, sixthWord, seventhWord, eighthWord)>0){
            
            if (strcmp(firstWord, "doneth")==0) {
                break;
            }else
                //Comments. Go to newline
                if (strcmp(firstWord, "#")==0) {
                    //break;
                }else if (strcmp(firstWord, "henceforth")==0) {
                    //Check for invalid input
                    char tokens[][TOKENS] = { 
                        "henceforth", 
                        "printeh",
                        "doneth",
                        "shalt",
                        "holde",
                        "ye",
                        "olde",
                        "thine",
                        "pluseth",
                        "minuseth",
                        "timeth",
                        "divideth",
                        "by",
                        "exulted"
                    };
                    
                    //Operand name is the second word.
                    strcpy(operandName, secondWord);
                    
                    for (i = 0; i < TOKENS; i++) {
                        if (strcmp(tokens[i], operandName) == 0)
                            return FALSE;
                    }
                    
                    
                    if (strcmp(thirdWord, "shalt")!=0) {
                        return FALSE;
                    }else if (strcmp(fourthWord, "holde")!=0) {
                        return FALSE;
                    }else{
                        
                        //*******************
                        //Value Assignment
                        //*******************
                        if( strcmp(fifthWord, "ye")==0 && strcmp(sixthWord, "olde")==0){
                            
                            //The seventh word is the value or the key to the value
                            strcpy(operatorOneName, seventhWord);
                            
                            //If the variable exists then find its value
                            if ((operatorOneNode = bistree_lookup(Tree, Tree->head, operatorOneName)) != NULL) {
                                operatorOneValue = operatorOneNode->var;
                            }else{
                                //Otherwise the name is an integer
                                sscanf(operatorOneName, "%d", &operatorOneValue);
                            }
                            
                            //Assign the value of operatorOneValue to operand
                            //See if the operand already has a node.
                            if ((operandNode = bistree_lookup(Tree, Tree->head, operandName)) != NULL) {
                                //If it does then change the value
                                operandNode->var = (operatorOneValue);
                            }else{
                                //Else, add the node and update value
                                bistree_insert(Tree, operandName, operatorOneValue);
                            }
                            
                        }
                        
                        //*******************
                        //Variable Assignment
                        //*******************
                        if (strcmp(fifthWord, "thine") == 0) {
                            strcpy(operatorOneName, sixthWord);
                            //If the variable exists then find its value
                            if ((operatorOneNode = bistree_lookup(Tree, Tree->head, operatorOneName)) != NULL) {
                                operatorOneValue = operatorOneNode->var;
                            }else{
                                //Otherwise the name is an integer
                                sscanf(operatorOneName,"%d", &operatorOneValue);
                            }
                            
                            //Assign the value of operatorOneValue to operand
                            //See if the operand already has a node.
                            if ((operandNode = bistree_lookup(Tree, Tree->head, operandName)) != NULL) {
                                //If it does then change the value
                                operandNode->var = (operatorOneValue);
                            }else{
                                //Else, add the node and update value
                                bistree_insert(Tree, operandName, operatorOneValue);
                            }
                        }
                        
                        
                        //********************************
                        //Arithmetic Assignment Statements
                        //********************************
                        
                        
                        //If the fifth word not printeth then an operation will take place
                        else if(strcmp(firstWord, "printeth") != 0){
                            
                            //The fifth word contains either the key to the value or the value of the first operator
                            strcpy(operatorOneName, fifthWord);
                            
                            //This is a special case because dividith takes two words
                            if (strcmp(sixthWord, "dividth")==0) {
                                strcpy(operatorTwoName , eighthWord);
                                strcpy(operation, sixthWord);
                            }else{
                                strcpy(operatorTwoName, seventhWord);
                                strcpy(operation, sixthWord);
                            }
                            
                            //*****
                            //At this point we know that the seventh word holds the operation so lets check if the operatorNames already have values
                            //Or if they're integers
                            //
                            
                            //*********************    
                            //For operatorOneVALUE!
                            //If the variable exists then find its value
                            if ((operatorOneNode = bistree_lookup(Tree, Tree->head, operatorOneName)) != NULL) {
                                operatorOneValue = operatorOneNode->var;
                            }else{
                                //Otherwise the name is an integer
                                sscanf(operatorOneName,"%d", &operatorOneValue);
                            }
                            
                            //**********************
                            //For operatorTwoValue
                            //Check if the second operator is a variable or not
                            if ((operatorTwoNode = bistree_lookup(Tree, Tree->head, operatorTwoName)) != NULL) {
                                operatorTwoValue = operatorTwoNode->var;
                            }else{
                                sscanf(operatorTwoName,"%d", &operatorTwoValue);
                            }
                            
                            //******
                            //ADDING
                            //Now we check which operation to do
                            if (strcmp(operation, "pluseth")==0) {
                                
                                strcpy(operatorTwoName, eighthWord);
                                
                                //See if the operand already has a node.
                                if ((operandNode = bistree_lookup(Tree, Tree->head, operandName)) != NULL) {
                                    //If it does then change the value
                                    operandNode->var = (operatorOneValue + operatorTwoValue);
                                }else{
                                    //Else, add the node and update value
                                    bistree_insert(Tree, operandName, (operatorOneValue+operatorTwoValue));
                                }
                                
                                //***********
                                //SUBTRACTING
                                //***********
                            }else if (strcmp(operation, "minuseth")==0){
                                //Same Procedure, just subtracting
                                
                                //preform the operation and store
                                if ((operandNode = bistree_lookup(Tree, Tree->head, operandName)) != NULL) {
                                    operandNode->var = (operatorOneValue - operatorTwoValue);
                                }else{
                                    bistree_insert(Tree, operandName, (operatorOneValue-operatorTwoValue));
                                }
                                
                            }
                            
                            //***********
                            //MULTIPLYING
                            //***********
                            else if (strcmp(operation, "timeth")==0){
                                //Same Procedure, just multiplying
                                
                                if ((operandNode = bistree_lookup(Tree, Tree->head, operandName)) != NULL) {
                                    operandNode->var = (operatorOneValue * operatorTwoValue);
                                }else{
                                    bistree_insert(Tree, operandName, (operatorOneValue*operatorTwoValue));
                                }
                                
                                //******************
                                //RAISING TO A POWER
                                //******************
                            }else if (strcmp(operation, "exulted")==0){
                                //Same Procedure, just raising to a power
                                
                                if ((operandNode = bistree_lookup(Tree, Tree->head, operandName)) != NULL) {
                                    operandNode->var = (int)pow(operatorOneValue, operatorTwoValue);
                                }else{
                                    bistree_insert(Tree, operandName, (int)pow(operatorOneValue, operatorTwoValue));
                                }
                                
                                //********
                                //DIVIDING
                                //********
                            }else if (strcmp(operation, "divideth")==0){
                                //Same Procedure, just raising to a power
                                //Check if the eigthword is a variable or not
                                
                                if ((operandNode = bistree_lookup(Tree, Tree->head, operandName)) != NULL) {
                                    
                                    if(operatorTwoValue==0){
                                        return FALSE;
                                    }else{
                                    operandNode->var = operatorOneValue/operatorTwoValue;
                                    }
                                }else{
                                    if (operatorTwoValue==0) {
                                        return FALSE;
                                    }
                                    bistree_insert(Tree, operandName, operatorOneValue/operatorTwoValue);
                                }
                            }
                        }
                    }
                }else{
                    if ((operandNode = bistree_lookup(Tree, Tree->head, secondWord)) != NULL) {
                        printf("%s = %d\n", secondWord, operandNode->var);
                    }else{
                        printf("ERROR: Variable Not Defined\n");
                    }
                }
            
            
            
        }
    }
    
    //Close file
    fclose(inputFile);
    return TRUE;
}



