/**************************************************************************************************/
/*
 * File: bitree.c
 * Author: Kevin Klug
 * Date: 28 October 2011
 *
 * Description: Basic functions for the implementataion of a binary search tree
 *
 */	
/**************************************************************************************************/

#include <stdlib.h>
#include <string.h>
#include "bitree.h"

/*************************************************************/
//The following are functions implementing a binary search tree
/*************************************************************/

//*********************/
//HIGHER LEVEL FUNCTIONS

//************************************/
//Bitree_init intializes a binary tree
//************************************/
void bitree_init(BiTree *tree){
    
    tree->size = 0;
    tree->head = NULL;
    
    //Tree Balance is one when it is balanced
    tree->balanced = 1;
    
}
//*************************************************/
//Destroys the Binary search tree specified by tree.
//*************************************************/
void bitree_destroy(BiTree *tree){
    bitree_rem_left(tree, NULL);
}

//***********************************************/
//Removes the element in the specified binary tree
//***********************************************/
bool bitree_remove(BiTree *tree, char *word);


//************************************************************************/
//Returns a pointer to the location of the node that matches an input word.
//Will return NULL if the node cannot be found
//************************************************************************/
BiTree* bitree_lookup(BiTree *tree, char *word);

//********************/
//LOWER LEVEL FUNCTIONS

//********************************************************************/
//Insert a node to the left pointer of the specified BiTreeNode of tree
//********************************************************************/
bool bitree_ins_left(BiTree *tree, BiTreeNode *node, char *varName, int val){
    char *new_word;
    BiTreeNode *new_node, **position;
    
    //Check if given node is null
    if( node == NULL){
        
        //Only allow insertion at root of an empty tree
        if(bistree_size(tree) > 0){
            return FALSE;
        }
        position = &tree->head;
    }
    
    //Insert as normal
    else {
        //Check to make sure there isn't something already in the left pointer of given node
        if(bitree_left(node) != NULL){
            return FALSE;
        }
        position = &node->left;
    }
    
    //Allocate memory for the new node
    if((new_node = (BiTreeNode *)malloc(sizeof(BiTreeNode)))==NULL) return FALSE;
    
    if((new_word = (char *)malloc(strlen(varName))) == NULL) return FALSE;
    strcpy(new_word, varName);
    new_node->varName = new_word;
    new_node->var= val;
    new_node->left = NULL;
    new_node->right = NULL;
    *position = new_node;
    
    //Important for AVL TREE implementation
    new_node->balance_factor = 0;
    
    //Assign the parent of the new node
    new_node->parent = node; 
    new_node->left = NULL;
    new_node->right = NULL;
    
    //Increase the size of the tree
    tree->size++;
    
    return TRUE;
    
}


//*********************************************************************/
//Insert a node to the right pointer of the specified BiTreeNode of tree
//*********************************************************************/

bool bitree_ins_right(BiTree *tree, BiTreeNode *node, char *varName, int val){
    char *new_word;
    BiTreeNode *new_node, **position;
    
    //Check if given node is null
    if( node == NULL){
        //Only allow insertion at root of an empty tree
        
        if(bistree_size(tree) > 0){
            return FALSE;
        }
        position = &tree->head;
    }
    
    //Insert as normal
    else {
        //Check to make sure there isn't something already in the left pointer of given node
        if(bitree_right(node) != NULL){
            return FALSE;
        }
        position = &node->right;
    }
    
    //Allocate memory for the new node
    if((new_node = (BiTreeNode *)malloc(sizeof(BiTreeNode)))==NULL) return FALSE;
    
    if((new_word = (char *)malloc(strlen(varName))) == NULL) return FALSE;
    strcpy(new_word, varName);
    new_node->varName = new_word;
    new_node->var = val;
    new_node->left = NULL;
    new_node->right = NULL;
    *position = new_node;
    
    //Important for AVL TREE implementation
    new_node->balance_factor = 0;
    
    //Assign the parent of the new node
    new_node->parent = node;
    new_node->left = NULL;
    new_node->right = NULL;
    
    //Increase the size of the tree
    tree->size++;
    
    return TRUE;
    
}

//**********************************************************************/
//Remove the node in the left pointer of the specified BiTreeNode of tree
//**********************************************************************/

bool bitree_rem_left(BiTree *tree, BiTreeNode *node){
    
    BiTreeNode **position;
    
    //Can't remove an element from an empty tree
    if(bistree_size(tree) == 0) return FALSE;
    
    if (node == NULL) {
        position = &tree->head;
    }else{
        position = &node->left;
    }
    
    if(*position != NULL){
        //Recursively remove the left and right of node->left
        bitree_rem_left(tree, *position);
        bitree_rem_right(tree, *position);
        

        free(*position);
        
        *position = NULL;
        
        //Removal success, decrease size of tree
        tree->size--;
    }
    return TRUE;
}

//***********************************************************************/
//Remove the node in the right pointer of the specified BiTreeNode of tree
//***********************************************************************/
bool bitree_rem_right(BiTree *tree, BiTreeNode *node){
    BiTreeNode **position;
    
    //Can't remove an element from an empty tree
    if(bistree_size(tree) == 0) return FALSE;
    
    //Ensure that the node exists, if not remove the head(root) of the tree
    if (node == NULL) {
        position = &tree->head;
    }else{
        position = &node->right;
    }
    
    //Recursively remove the left and right of node->right
    if(*position != NULL){
        bitree_rem_left(tree, *position);
        bitree_rem_right(tree, *position);
        
        //Free memory of the word
        free(node->right->varName);
        //Free node->right
        free(*position);
        
        *position = NULL;
        
        //Removal success, decrease size of tree
        tree->size--;
    }
    return TRUE;
}

//**************************/
//Return the size of the tree
//**************************/
int bistree_size( BiTree *tree){
    return tree->size;
}

//*****************************************************/
//Return the position of the left node of the given node
//*****************************************************/
BiTreeNode* bitree_left(BiTreeNode *node){
    return node->left;
}

//******************************************************/
//Return the position of the right node of the given node
//******************************************************/
BiTreeNode* bitree_right(BiTreeNode *node){
    return node->right;
}


/**************************************************************************************************/

