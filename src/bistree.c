/**************************************************************************************************/
/*
 * File: bistree.c
 * Author: Kevin Klug
 * Date: 29 September 2011
 *
 * Description: Basic functions for the implementation of an AVL Binary Search Tree
 *
 */
/**************************************************************************************************/

#include "global.h"
#include "bistree.h"
#include <stdlib.h>
#include <string.h>
/**************************************************************************************************/

//**********************
//HIGHER LEVEL FUNCTIONS

//Initalize a Binary Search Tree
void bistree_init(BisTree *tree){
    bitree_init(tree);
}

//Destroy a Binary Search Tree
void bistree_destroy(BisTree *tree){
    bitree_destroy(tree);
}

//Inserts a node in the specified binary tree
bool bistree_insert(BiTree *tree, char *word, int val){
    bool balanced = 0;
    return insert(tree, tree->head, word, &balanced, val);
}

//Function that finds the proper place to insert a node in a balanced AVL tree
bool insert(BisTree *tree, BiTreeNode *node, char *word, int *balanced, int val){
    
    int compare = 0;
    
    if (node == NULL) {
        return bitree_ins_left(tree, NULL, word, val);
    }
    
    //Use string compare to place the words in the tree alphabetically.
    //Strcmp will return a - when the first found letter is smaller than the current node's word and + when greater
    compare = strcmp(word, node->varName);
    
    //The same word has been found. Return an Error
    if (compare == 0) {
        return FALSE;
    }else if (compare < 0) {
        
        //Simply insert to the left pointer of node if the left most node is null
        if (node->left == NULL) {
            if (bitree_ins_left(tree, node, word, val)==FALSE) {
                return FALSE;
            }
            //Tree may not be balanced anymore.
            *balanced = 0;
            
        //Else, Keep recursivley calling insert to find the appropriate entry point
        }else if (insert(tree, node->left, word, balanced, val) != TRUE){
            return FALSE;
        }
                
    }
    //The word to be inserted is alphabetically larger than the current node
    else if (compare  > 0){
        if (node->right == NULL) {
            if (bitree_ins_right(tree, node, word, val)==FALSE) {
                return FALSE;
            }
            *balanced = 0;
        }
        //Keep trying to find the correct insertion point by recursivly calling the current function
        else{
            if (insert(tree, node->right, word, balanced, val)!=TRUE) {
                return FALSE;
            }
        }
                        
    }
    
    return TRUE;
}


//Removes a node in the specified binary tree
bool bistree_remove(BisTree *tree);

//Returns a pointer to the location of the node that stores given word
BiTreeNode * bistree_lookup(BisTree *tree, BiTreeNode *node, char *word){
    int compare = 0;
    
    if (node == NULL) {
        return NULL;
    }
    
    compare = strcmp(word, node->varName);
    
    if (compare < 0) {
        return bistree_lookup(tree, node->left, word);
    }
    
    else if (compare > 0) {
        return bistree_lookup(tree, node->right, word);
    }
    
    else if (compare == 0) {
        return node;
    }
    
    return NULL;
}

//Returns the size of the binary search tree
int bistree_size(BisTree *tree);




