
/**************************************************************************************************/
/*
 * File: thenecessary.h
 * Author: Kevin Klug
 * Date: 10 November 2011
 *
 * Description: High level functions to implement the yeoldecalc program
 *
 */
/**************************************************************************************************/

#include "global.h"
#include "bitree.h"

/**************************************************************************************************/
#ifndef THENECESSARY_H
#define THENECESSARY_H

bool yeoldecalc( char *inputFile, BiTree *Tree );

#endif

